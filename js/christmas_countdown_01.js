$(function () {
  function countdown() {
    var now = new Date();
    var year = isPastChristmas(now) ? now.getFullYear() + 1 : now.getFullYear();
    var eventDate = new Date(year + "-12-25T00:00:00");
    var currentTime = now.getTime();
    var evenTime = eventDate.getTime();

    var remTime = evenTime - currentTime;

    var sec = Math.floor(remTime / 1000);
    var min = Math.floor(sec / 60);
    var hur = Math.floor(min / 60);
    var day = Math.floor(hur / 24);

    hur %= 24;
    min %= 60;
    sec %= 60;

    hur = hur < 10 ? "0" + hur : hur;
    min = min < 10 ? "0" + min : min;
    sec = sec < 10 ? "0" + sec : sec;

    $(".seconds").text(sec);
    $(".minutes").text(min);
    $(".hours").text(hur);
    $(".days").text(day);

    setTimeout(countdown, 1000);
  }

  function isPastChristmas(now) {
    return now.getMonth() > 11 && now.getDay() > 24;
  }

  countdown();
});
